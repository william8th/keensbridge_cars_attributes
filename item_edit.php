<script type="text/javascript">
    $(document).ready(function(){
        $("#make").change(function(){
            var make_id = $(this).val();
            var url = '<?php echo osc_ajax_plugin_url('cars_attributes/ajax.php') . '&makeId='; ?>' + make_id;
            var result = '';
            if(make_id != '') {
                $("#model").attr('disabled',false);
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function(data){
                        var length = data.length;
                        if(length > 0) {
                            result += '<option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>';
                            for(key in data) {
                                result += '<option value="' + data[key].pk_i_id + '">' + data[key].s_name + '</option>';
                            }
                        } else {
                            result += '<option value=""><?php _e('No results', 'cars_attributes'); ?></option>';
                        }
                        $("#model").html(result);
                    }
                 });
             } else {
                result += '<option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>';
                $("#model").attr('disabled',true);
                $("#model").html(result);
             }
        });
    });
</script>
<h2><?php _e('Car details', 'cars_attributes') ; ?></h2>
<div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_make') != '' ) {
                $detail['fk_i_make_id'] = Session::newInstance()->_getForm('pc_make');
            }
        ?>
        <label><?php _e('Make', 'cars_attributes'); ?></label>
        <select name="make" id="make" >
            <option value=""><?php _e('Select a make', 'cars_attributes'); ?></option>
            <?php foreach($makes as $a){ ?>
            <option value="<?php echo $a['pk_i_id']; ?>" <?php if(@$detail['fk_i_make_id'] == $a['pk_i_id']) { echo 'selected'; } ?>><?php echo $a['s_name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_model') != '' ) {
                $detail['fk_i_model_id'] = Session::newInstance()->_getForm('pc_model');
            }
        ?>
        <label><?php _e('Model', 'cars_attributes'); ?></label>
        <select name="model" id="model">
            <option value="" selected><?php _e('Select a model', 'cars_attributes'); ?></option>
            <?php foreach($models as $a) { ?>
            <option value="<?php echo $a['pk_i_id']; ?>" <?php if(@$detail['fk_i_model_id'] == $a['pk_i_id']) { echo 'selected'; } ?>><?php echo $a['s_name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="row _200">
        <?php if( Session::newInstance()->_getForm('pc_color') != '' ) {
            $detail['fk_color_id'] = Session::newInstance()->_getForm('pc_color');
        } ?>
            <div class="tabber">
                <div class="tabbertab">
                    <p>
                        <label><?php _e('Color', 'cars_attributes'); ?></label>
                        <select name="color" id="color">
                            <option value="" selected><?php _e('Select a color', 'cars_attributes'); ?></option>
                            <?php foreach($colors as $c) { ?>
                            <option value="<?php echo  $c['pk_i_id']; ?>" <?php if(@$detail['fk_i_color_id'] == $c['pk_i_id']) { echo 'selected'; } ?>><?php echo @$c['s_name']; ?></option>
                            <?php } ?>
                        </select>
                    </p>
                </div>
            </div>
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_year') != '' ) {
                $detail['i_year'] = Session::newInstance()->_getForm('pc_year');
            }
        ?>
        <label><?php _e('Year', 'cars_attributes'); ?></label>
        <input type="text" name="year" id="year" value="<?php echo @$detail['i_year']; ?>" size=4 />
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_mileage') != '' ) {
                $detail['i_mileage'] = Session::newInstance()->_getForm('pc_mileage');
            }
        ?>
        <label><?php _e('Mileage (km)', 'cars_attributes'); ?></label>
        <input type="text" name="mileage" id="mileage" value="<?php echo @$detail['i_mileage']; ?>" />
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_engine_size') != '' ) {
                $detail['i_engine_size'] = Session::newInstance()->_getForm('pc_engine_size');
            }
        ?>
        <label><?php _e('Engine size (cc)', 'cars_attributes'); ?></label>
        <input type="text" name="engine_size" id="engine_size" value="<?php echo @$detail['i_engine_size']; ?>" />
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_transmission') != '' ) {
                $detail['e_transmission'] = Session::newInstance()->_getForm('pc_transmission');
            }
        ?>
        <label><?php _e('Transmission', 'cars_attributes'); ?></label>
        <select name="transmission" id="transmission">
            <option value="MANUAL" <?php if(@$detail['e_transmission'] == 'MANUAL') { echo 'selected'; } ?>><?php _e('Manual', 'cars_attributes'); ?></option>
            <option value="AUTO" <?php if(@$detail['e_transmission'] == 'AUTO') { echo 'selected'; } ?>><?php _e('Auto', 'cars_attributes'); ?></option>
        </select>
    </div>
    <div class="row _200">
        <?php
            if( Session::newInstance()->_getForm('pc_seller') != '' ) {
                $detail['e_seller'] = Session::newInstance()->_getForm('pc_seller');
            }
        ?>
        <label><?php _e('Seller', 'cars_attributes'); ?></label>
        <select name="seller" id="seller">
            <option value="DEALER" <?php if(@$detail['e_seller'] == 'DEALER') { echo 'selected'; } ?>><?php _e('Dealer', 'cars_attributes'); ?></option>
            <option value="OWNER" <?php if(@$detail['e_seller'] == 'OWNER') { echo 'selected'; } ?>><?php _e('Owner', 'cars_attributes'); ?></option>
        </select>
    </div>
</div>
<script type="text/javascript">
    tabberAutomatic();
</script>
