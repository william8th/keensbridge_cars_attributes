<h2><?php _e('Car details', 'cars_attributes') ; ?></h2>
<table style="margin-left: 20px;">
    <?php if( !empty($detail['s_make']) ) { ?>
    <tr>
        <td width="150px"><label><?php _e('Make', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo @$detail['s_make']; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['s_model']) ) { ?>
    <tr>
        <td width="150px"><label><?php _e('Model', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo @$detail['s_model']; ?></td>
    </tr>
    <?php } ?>
    <?php if (! empty($detail['s_color'])) { ?>
    <tr>
        <td width="150px"><label><?php _e('Color', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo @$detail['s_color']; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['i_year']) ) { ?>
    <tr>
        <td width="150px"><label><?php _e('Year', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo $detail['i_year']; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['i_mileage']) ) { ?>
    <tr>
        <td width="150px"><label><?php _e('Mileage', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo @$detail['i_mileage']; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['i_engine_size']) ) { ?>
    <tr>
        <td width="150px"><label><?php _e('Engine size (cc)', 'cars_attributes'); ?></label></td>
        <td width="150px"><?php echo @$detail['i_engine_size']; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['e_transmission']) ) { ?>
    <tr>
        <?php $transmission = array('MANUAL' => __('Manual', 'cars_attributes'), 'AUTO' => __('Auto', 'cars_attributes')); ?>
        <td width="150px"><label><?php _e('Transmission', 'cars_attributes'); ?></label></td>
        <td width="150px"><label><?php echo $transmission[$detail['e_transmission']]; ?></td>
    </tr>
    <?php } ?>
    <?php if( !empty($detail['e_seller']) ) { ?>
    <tr>
        <?php $seller = array('DEALER' => __('Dealer', 'cars_attributes'), 'OWNER' => __('Owner', 'cars_attributes')); ?>
        <td width="150px"><label><?php _e('Seller', 'cars_attributes'); ?></label></td>
        <td width="150px"><label><?php echo $seller[$detail['e_seller']]; ?></td>
    </tr>
    <?php } ?>
</table>